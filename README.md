# Microsoft Azure

## Creating Resource Group
    * login into your Azure Portal
    * Search for Resoruce Groups in the search bar and click on same
    * Click On create button
    * Select your Subscription
    * Define a name for your Resource Group
    * Choose a Default Location
    * Click on Next Tags
    * Click on Review and create
    * Click on Create button

## Creating a windows based virtual machine
    * Search for Virtual Machines in the search bar and click on same
    * Click on create -> Virtual Machine
    * Select your Subscription and Resoruce Group
    * Define a name for your VM
    * Choose your respective Region
    * To select Image, Click on dropdown and select - Windows Server 2019 Datacenter - Gen2
    * To Define the size of Virtual Machine, click on see all sizes - Select b2s and Click on Select button at bottom left corner
    * Define your credentials now
    * Click on Review and Create
    * Create

## Creating a windows based virtual machine
    * Search for Virtual Machines in the search bar and click on same
    * Click on create -> Virtual Machine
    * Select your Subscription and Resoruce Group
    * Define a name for your VM
    * Choose your respective Region
    * To select Image, Click on dropdown and select - Ubuntu 18.04 LTS
    * To Define the size of Virtual Machine, click on see all sizes - Select b2s and Click on Select button at bottom left corner
    * Define your credentials now
    * Click on Review and Create
    * Create


## Installing Web Server in Linux Machine
    * Switch to root user
        sudo su
    * Update repo list
        apt-get update
    * Install Apache2
        apt-get install apache2
    * Check the service status
        service apache2 status

## To allow the traffic on Port 80
    * Go to your VM page
    * In the left pane, click on Networking
    * Click on Inbound Rule
    * Define the destination port as 80
    * Define a name for your Rule
    * Click on Add


## Creating a Data Disk and Attaching it to VM
    * Go to you VM page
    * In the left pane, click on Disks
    * Click on Create and Attach Disk
    * Define a name for your Disk, change the type as Standard SSD, Update size to 10 GB
    * Click on Save button

    * Go to your VM
    * let's list block devices
        lsblk
    * You can see a 10 GB sized disk got added into the list
    * Creating file system
        mkfs -t ext4 /dev/sdc
    * Creating a mountpoint
        mkdir /data
        mount /dev/sdc /data
    * Now, whatever you will store in /data will get stored inside the new disk only

## Creating Storage Account 
    * Search for Storage Accounts in the search bar and click on same
    * Click on create 
    * Select your Subscription and Resoruce Group
    * Define a name for your Storage Account
    * Choose the Region
    * Define Redundancy Level as LRS
    * Next Review & Create
    * Click on Create button

## Creating DB reosurce
    * Go to Azure Portal
    * Search for Azure Database for PostgreSQL servers, and click on that
    * Click on Create, then select Single Server and click on Create corresponding to that
    * Select your Subscription and Resource Group
    * Define a name for your DB Resource
    * Select your Region
    * To define the size, click on Configure Server, under Compute + Storage
    * Click on Basic, set the configuration to minimum
    * Define your credentials for this Database
    * Click on Next Review and Create
    * Click on Create button

## Accessing Databse Server from Virtual Machine
    * Installing DB Client
        apt-get update
        apt-get instal postgresql-client
    * Now let's allow the connections from VM to the database
        * Go to your DB page
        * Click on Connection Security in left pane
        * Click yes corresponding to 'Allow Access to Azure Services'
        * Save it
    * Not accesing db
        * Get the psql command from Azure portal
        * Go to your DB page
        * Click on Connection Strings in left pane
        * Copy the command corresponding to PSQL
        * Execute it in your VM after replcaing, database name as - 'postgres' and your password
    * Once you got connected to db, to exit out of that
        \q


## Creating Cosmos DB
    * Go to Azure Portal
    * Search for Cosmos DB, and click on that
    * Click on Create, then select Azure Cosmos DB API for MongoDB and click on Create corresponding to that
    * Select your Subscription and Resource Group
    * Define a name for your DB Resource
    * Select your Region
    * Keep the capacity mode as Provisioned Throughput only
    * Click Next Global Distribution
    * Click Next Networking
    * Click Next Backup Policy
    * Click Next Encryption
    * Click Next Tags
    * Click Next Review and Create
    * Create

## Creating Database, Collection and Documets in Cosmos DB using Azure Portal
    * Go to your Cosmos DB page
    * In the left pane, click on Data Explorer
    * New Databse
    * Define a name for that
    * keep thorughput as manual
    * Click ok
    * TO CREATE COLLECTION
    * In the left pane, select the database you created
    * Click on New Collection
    * Under database name, click Use existing
    * From dropdown, select the DB you created
    * define name for collection
    * Keep it unsharded
    * Click on OK
    * TO CREATE DOCUMENT
    * In the left pane, select your database, then colletion followed which you created now
    * Clcik on Documents
    * New Document
    * You can provide some data in json format, and save it


## Creating Database, Collection and Documets in Cosmos DB using MongoCLI
    * Installing Mongo DB Client
        * Import Public Key 
		    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
        * Adding Repo Path
		    echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.0.list
	    * Update Repo List
		    apt-get update
	    * Install mongo shell
		    apt-get install -y mongodb-org-shell
    * Connecting to Mongo DB
        * Get the mongo shell command from Azure Portal
            * Go to DB page
            * In the left pane, click on Quick Start
            * Click on Mongo Shell
            * Copy the command provided there
            * Execute it in your vm after removing '.exe' from the command
    * To get list of dbs
        show dbs
    * To get list of collections
        show collections
    * To switch to db
        use db_name
    * To get the documents inside your collection
        db.collection_name.find()
    * To get the documents inside your collection in pretty format
        db.data.find().pretty()
    * To insert document 
        db.data.insert({ "someKey": "Value", "someOtherKey": "SomeOtherValue" })

## Key Vault
    * Go to Azure Portal
    * Search for Key Vaults and click on the same
    * Create
    * Select your Subscription and Resoruce Group
    * Define your Key Valut name
    * Choose the Region
    * Next Access Policies
    * Next networking
    * Next Tags
    * Next Review + Create
    * Create

    * CREATING SECRET
    * In the left pane, click on Secrets
    * Click on Generate/Import
    * Define a name and put a value for same
    * Click Create to save it

    * ENABLING MANAGED IDENTITY ON VM
    * Go to VM page
    * In the left pane, click on Identity
    * Now make it ON and save it

    * Let's assign permission to the VM 
        * Go to key vault page
        * In the left pane, click on Access Policies
        * Click on Add Access Policy
        * Then choose your respective permissions 
        * Click on None Selected under Select Principal
        * in the right pane, search for your VM and Select it
        * Click on Select Button below that
        * Click on ADD button
        * Click on Save button
    * Now execute your program, things will work for you (https://gitlab.com/synechron-azure-august-batch/keyvault-python)

## Installing Azure CLI 
    curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash

## Authorizing Azure CLI with service principal
    * Creating Service Prinicpal
        * Go To Azure Portal
        * Search for Azure Active Directory and click on that
        * In the left pane, click on App Registrations
        * New Registration
        * Define a name for App
        * Click on Register Button
    * Let's assign some permissions to this app
        * Open the subscriptions page
        * Click on your Subscription
        * In the left pane, click on Access Control
        * Click on Add Role Assignment
        * Select the Role as Key Vault Administrator
        * Click on Next
        * Click on Select Members
        * Now in the right pane, search for your App name and select it
        * Click on Select button
        * Click on Next
        * Click on Review + Assign
    * Let's get the credentials for the service prinicipal which we created and provided access to
        * Go To Azure Portal
        * Search for Azure Active Directory and click on that
        * In the left pane, click on App Registrations
        * Click on your APP name/ Service Principal
        * Over here, you will get Tenant ID and App ID, kindly note them down
        * In the left pane, click on Certificates & Secrets
        * Click on New Client Secret
        * Define description and expiry
        * Click on add button
        * Now in the secret list, you can see some value popping up, copy it and keep it safe, this is your Client Secret
    * Now we have everything to login into azure cli, let's log in
        az login --service-principal -u <app_id> -t <tenant_id> -p <client_secret>


## Creating Application Insights Resource
    * Goto Azure Portal
    * Search for Application Insights over there and click on same
    * Create
    * Choose your Subscription and Resource Group 
    * Define a name for your Application Insights Resource
    * Choose Region
    * Select your existing Log Analytics Workpace
    * Next Tags
    * Next Review and Create
    * Create

## Application Insights for Client Side Monitoring via Javascript
    * Follow the steps provided here [https://gitlab.com/synechron-azure-august-batch/application-insights-javascript]
    * To see the monitored data
    * Go to your Application Insights Resoruce page
    * In the left pane, Click on Performance
    * Then choose the Browser option over the graph area, to see your client side metrics
    * There you will see some records popping up over there based upon the traffic your webpage received.
    * You can drill into logs, click on View in Logs, and then put some queries like 'pageViews'


## Setting up Alerting on VM
    * Go to VM Page
    * In the left pane, click on Alerts
    * New Alert Rule
    * Under condition -> Add Condition
    * Search for Percentage CPU in the list on the right pane and select it
    * Define the threshold value as 40%
    * Click on Done
    * To define action over the condition specified, click on Add Action Groups
    * Create Action Group
    * Define the name for Action Group
    * Click Next Notifications
    * Under notification type - choose Email/SMS message/Push/Voice
    * Check the box stating email and provide the email address onw which you wish to receive notificaitons
    * Click on Ok
    * Define the name for notification channel you added
    * Click on Next Actions
    * Next Tags
    * Next Review + Create
    * Click on Create
    * Now on the Alert Rule page, Define the name for the rule created
    * Choose the severity as per the need
    * Click on Create Alert Rule

## To put stress on the VM
    * Install Stress utility
        ```
        apt-get install stress
        ```
    * Put stress
        ```
        stress --cpu 8 --timeout 400
        ```
    * Now, just wait for a while, you will see a notification popping in your inboxes with an alert trigerred

    
